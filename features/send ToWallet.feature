@wallets
Feature: Send money to mobile wallets
Scenario: Confirm send to mobile wallets functionality exists
 	Given user is on transfer page
 	When user clicks send to someone
 	Then user should see send to mobile wallets
	
Scenario: Transaction initiation for mobile wallets
 	Given user is on send to someone
 	When User clicks send to mobile wallet
 	Then user is prompted for source account

Scenario: Enter source account
	Given user is on details entry page
	When user clicks on source account
	And user selects source account
	Then allow user to select source account
	
Scenario: Enter destination account
	Given user is on details entry page
	When user clicks on recipient
	And user clicks to someone new
	Then prompt user to select network operator
	
Scenario: Select network operator country
	Given user is on network operator page
	When user clicks the country
	Then user should select operator country
	
Scenario: Select mobile wallet 
	Given user is on network operator page
	When user selects an wallet
	Then prompt user to enter wallet number
	
Scenario: Enter mobile wallet number
	Given user is on beneficiary details page
	When user enters mobile wallet number
	Then allow user to click add
	
Scenario: Select transaction currency
	Given user is on details entry page
	When user clicks on currency
	And user selects transaction currency
	Then allow user to select currency
	
Scenario: Enter Amount
	Given user is on details entry page
	When user clicks on amount
	Then user enters amount
	
Scenario: Enter Payment reason
	Given user is on details entry page
	When user enters payment reason
	And user clicks send
	Then allow user to proceed
	
Scenario: Confirm transaction details
	Given user is on review page
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	
Scenario: Complete transaction 
	Given user is on review page
	When user clicks pay
	Then complete transaction
	Then click done

