@toOwn
Feature: Send money transactions

Scenario: Confirm the functionality exists
	Given user is on homepage
	When user clicks transfer
	Then user should see send to own account

Scenario: Transaction initiation
	Given user is on transfer page
	When user clicks send to own account 
	Then user is prompted for source account
	
Scenario: Enter source account
	Given user is on details entry page
	When user clicks on source account
	And user selects source account
	Then allow user to select source account
	
Scenario: Enter destination account
	Given user is on details entry page
	When user clicks on destination account
	And user selects destination account
	Then allow user to select destination account
	
Scenario: Select transaction currency
	Given user is on details entry page
	When user clicks on currency
	And user selects transaction currency
	Then allow user to select currency
	
Scenario: Enter Amount
	Given user is on details entry page
	When user clicks on amount
	Then user enters amount
	
	Scenario: Enter Payment reason
	Given user is on details entry page
	When user enters payment reason
	And user clicks send
	Then allow user to proceed
	
Scenario: Review transaction details
	Given user is on review page
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then user clicks pay
	
Scenario: Confirm transaction details
	Given user completes transaction
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then check transaction reference
	Then click done

Scenario: Transaction without source account
	Given user is on transfer page
	When user clicks send to own account
	And user enters the destination account
	And user enters amount
	And user clicks send
	Then do not proceed
	
Scenario: Transaction without destination account
	Given user goes back to transfer page
	When user clicks send to own account
	And user enters the source account
	And user enters amount
	And user clicks send
	Then app should return error
	
Scenario: Transaction without amount
	Given user goes back to transfer page
	When user clicks send to own account
	And user enters the source account
	And user enters the destination account
	Then do not proceed
