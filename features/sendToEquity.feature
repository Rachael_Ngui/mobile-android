@equity
Feature: Send money to equity account
 Scenario: Confirm functionality exists
 	Given user is on transfer page
 	When user clicks send to someone
 	Then user should see send to equity
	
Scenario: Transaction initiation
 	Given user is on send to someone
 	When User clicks send to equity
 	Then user is prompted for source account
 	
Scenario: Enter source account
	Given user is on details entry page
	When user clicks on source account
	And user selects source account
	Then allow user to select source account
	
Scenario: Enter destination account
	Given user is on details entry page
	When user clicks on recipient
	And user clicks to someone new
	And user enters beneficiary
	Then allow user to click add
	
Scenario: Select transaction currency
	Given user is on details entry page
	When user clicks on currency
	And user selects transaction currency
	Then allow user to select currency
	
Scenario: Enter Amount
	Given user is on details entry page
	When user clicks on amount
	Then user enters amount
	
Scenario: Enter Payment reason
	Given user is on details entry page
	When user enters payment reason
	And user clicks send
	Then allow user to proceed
	
Scenario: Review transaction details
	Given user is on review page
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then user clicks pay
	
Scenario: Confirm transaction details
	Given user completes transaction
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then check transaction reference
	Then click done
	
Scenario: Complete transaction 
	Given user is on review page
	When user clicks pay
	Then complete transaction
	Then click done
	
Scenario: Transaction without source account
	Given user is on transfer page
 	When user clicks send to someone
	And User clicks send to equity
	And user enters the recipient
	And user enters amount
	And user clicks send
	Then do not proceed
	
Scenario: Transaction without beneficiary account
	Given user is on send to someone
	When User clicks send to equity
	And user enters the source account
	And user enters amount
	And user clicks send
	Then do not proceed
	
Scenario: Transaction without amount
	Given user is on send to someone
	When User clicks send to equity
	And user enters the source account
	And user enters the recipient
	And user clicks send
	Then do not proceed
 
