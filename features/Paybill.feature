 # Paybills transactions
 
 Feature: Paybill
 Scenario: confirm functionality exists
  	Given user is on homepage
 		When user clicks transfer
 		Then user should see paybill functionality
 		
 Scenario: Transaction initiation
	Given user is on transfer page
	When user clicks paybill 
	Then prompt user for source account 
	
 Scenario: Enter source account
	Given user is on details entry page
	When user clicks source account
	And user selects source account
	Then allow user to select source account
	
 Scenario: Enter biller details
	Given user is on details entry page
	When user clicks select biller
	Then direct user to biller categories
	
 Scenario: Select biller
 	Given user is on billers page
 	When user selects biller category
 	Then allow user to select biller
 	
 Scenario: Enter biller number
 	Given user is on biller account page
 	When user enters biller number
 	Then allow user to proceed