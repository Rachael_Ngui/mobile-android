 # Buy airtime feature 
@airtime
Feature: Buy Airtime

Scenario: Confirm functionality exists
 Given user is on homepage
 When user clicks transfer
 And user scrolls down
 Then user should see buy airtime 
 
 Scenario: Transaction initiation
	Given user is on transfer page
	When user clicks buy airtime 
	Then prompt user for source account
		
 Scenario: Enter source account
	Given user is on details entry page
	When user clicks on the source account
	And user selects source account
	Then allow user to select source account
	
 Scenario: Enter beneficiary mobile number
	Given user is on details entry page
	When user clicks on beneficiary 
	And user clicks to someone new
	Then prompt user to select network operator
	
 Scenario: Select recipient mobile operator
	Given user is on network operator page
	When user selects the mobile operator
	Then prompt user to enter mobile number
	
 Scenario: Enter recipient mobile number
	Given user is on beneficiary details page
	When user enters recipient mobile number
	Then allow user to click add
	
 Scenario: Enter Amount
	Given user is on details entry page
	When user enters amount
	Then proceed to enter payment reason
	
 Scenario: Enter Payment reason
	Given user is on details entry page
	When user enters payment reason
	And user clicks send
	Then allow user to proceed
	
 Scenario: Review transaction details
	Given user is on review page
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then user clicks buy airtime button
	
	Scenario: Confirm transaction details
	Given user completes transaction
	Then check source account
	Then check destination account
	Then check amount
	Then check charges
	Then check transaction reference
	Then click done
	
 