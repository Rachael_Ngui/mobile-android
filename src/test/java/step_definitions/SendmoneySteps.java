package step_definitions;

import Actions.SendmoneyActions;
import cucumber.api.java.en.*;

public class SendmoneySteps {
	SendmoneyActions send_actions = new SendmoneyActions();
	
	@Given("^user is on homepage$")
	public void user_is_on_homepage() throws InterruptedException {
		//after user has logged in wait for features to load
		Thread.sleep(10);
	}
	
	@When("^user clicks transfer$")
	public void user_clicks_transfer() {
		send_actions.click_transfer();
	}

	@Then("^user should see send to own account$")
	public void user_should_see_send_to_own_account() {
		send_actions.findsendtoown();
	}
	@Given("^user is on transfer page$")
	public void user_is_on_transfer_page() throws InterruptedException {
	    Thread.sleep(3000); // wait for features to load
	}

	@When("^user clicks send to own account$")
	public void user_clicks_send_to_own_account() {
		send_actions.click_sendtoown();
	}

	@Then("^user is prompted for source account$")
	public void user_is_prompted_for_source_account() {
		send_actions.confirm_sourceaccount();
	}
	@Given("^user is on details entry page$")
	public void user_is_on_details_entry_page() throws InterruptedException {
	   Thread.sleep(2000);
	}
	@When("^user clicks on source account$")
	public void user_clicks_on_source_account() {
		send_actions.click_source();
	}

	@And("^user selects source account$")
	public void user_selects_source_account() throws InterruptedException {
	    Thread.sleep(5000);
	    send_actions.select_source();
	}

	@Then("^allow user to select source account$")
	public void allow_user_to_select_source_account() {
	    System.out.println("source account selected successfully");
	}
	@When("^user clicks on destination account$")
	public void user_clicks_on_destination_account() throws InterruptedException {
		 Thread.sleep(1000);
		 send_actions.click_destination();
	}

	@And("^user selects destination account$")
	public void user_selects_destination_account() throws InterruptedException{
		Thread.sleep(5000);
		send_actions.select_dest();
	}

	@Then("^allow user to select destination account$")
	public void allow_user_to_select_destination_account() {
		System.out.println("destination account selected successfully");
	}
	@When("^user clicks on currency$")
	public void user_clicks_on_currency() {
		send_actions.click_currency();
	}

	@And("^user selects transaction currency$")
	public void user_selects_transaction_currency() {
		send_actions.select_currency("KES");
	}

	@Then("^allow user to select currency$")
	public void allow_user_to_select_currency() {
	    
	}

	@When("^user clicks on amount$")
	public void user_clicks_on_amount()  {
	    
	}
	
	@And("^user enters amount$")
	public void user_enters_amount() {
		send_actions.enter_amount("150.55");
	}
	
	@Then("^allow user to enter amount$")
	public void allow_user_to_enter_amount()  {
	    System.out.println("amount entered");
	}
	
	@When("^user clicks send$")
	public void user_clicks_send() throws InterruptedException {
		Thread.sleep(3000);
		send_actions.click_send();
	}
	@Then("^allow user to proceed$")
	public void allow_user_to_proceed() {
		send_actions.reviewdetails();
	}
	
	@Given("^user is on review page$")
	public void user_is_on_review_page() throws InterruptedException {
		send_actions.reviewdetails();
	}

	@Then("^check source account$")
	public void check_source_account() {
		send_actions.check_from();
	}

	@Then("^check destination account$")
	public void check_destination_account() {
		send_actions.check_to();
	}

	@Then("^check amount$")
	public void check_amount() {
		send_actions.check_amount();
	}

	@Then("^check charges$")
	public void check_charges() {
		send_actions.check_charge();
	}
	@When("^user clicks pay$")
	public void user_clicks_pay() {
		send_actions.click_pay();
	}
	@Then("^user completes transaction$")
	public void user_completes_transaction() throws InterruptedException {
		Thread.sleep(15000);
		send_actions.success();
	}
	@Then("^click done$")
	public void click_done() {
		send_actions.click_done();
	}

	@When("^user enters the destination account$")
	public void user_enters_the_destination_account() throws InterruptedException {
		send_actions.click_destination();
		send_actions.select_dest();
	}

	@Then("^do not proceed$")
	public void do_not_proceed() {
		send_actions.send_inactive();
	}
	@When("^user goes back to transfer page$")
	public void user_goes_back_to_transfer_page()  {
		send_actions.click_cancel();
	}
	@Then("^app should return error$")
	public void app_should_return_error() {
		send_actions.confirm_errormessage("Please enter the destination account");
	}
	
	@When("^user enters the source account$")
	public void user_enters_the_source_account() throws Throwable {
		send_actions.click_source();
		send_actions.select_source();
	}
	
	@When("^user clicks send to someone$")
	public void user_clicks_send_to_someone() throws InterruptedException {
		send_actions.click_transfer();
		Thread.sleep(2000);
		send_actions.click_sendtosomeone();
	}

	@Then("^user should see send to equity$")
	public void user_should_see_send_to_equity() {
		send_actions.confirm_sendtoequity();
	}

	@Given("^user is on send to someone$")
	public void user_is_on_send_to_someone() throws InterruptedException {
		Thread.sleep(1000);
	}

	@When("^User clicks send to equity$")
	public void user_clicks_send_to_equity() {
		send_actions.click_sendtoequity();
	}
	@When("^user clicks on recipient$")
	public void user_clicks_on_recipient(){
		send_actions.click_recipient();
	}
	@And("^user clicks to someone new$")
	public void user_clicks_to_someone_new(){
		send_actions.click_someonenew();
	}

	@And("^user enters beneficiary$")
	public void user_enters_beneficiary() {
		send_actions.enter_beneficiary("0350191765307");
	}

	@Then("^allow user to click add$")
	public void allow_user_to_click_add() {
		send_actions.click_add();
	}
	
	@And("^user enters payment reason$")
	public void user_enters_payment_reason()  {
		send_actions.payment_reason("Test1");
	}
	@When("^user enters the recipient$")
	public void user_enters_the_recipient() {
		send_actions.click_recipient();
		send_actions.click_someonenew();
		send_actions.enter_beneficiary("0350191765307");
		send_actions.click_add();
	}
	@Then("^user should see send to mobile wallets$")
	public void user_should_see_send_to_mobile_wallets() {
		send_actions.confirm_wallets();
	}
	@When("^User clicks send to mobile wallet$")
	public void user_clicks_send_to_mobile_wallet() {
		send_actions.click_wallets();
	}
	@Then("^prompt user to select network operator$")
	public void prompt_user_to_select_network_operator(){
		send_actions.confirm_operator();
	}
	@Given("^user is on network operator page$")
	public void user_is_on_network_operator_page() {
	   // already confirm
	}

	@When("^user clicks the country$")
	public void user_clicks_the_country() {
		send_actions.operator_country();
	}

	@Then("^user should select operator country$")
	public void user_should_select_operator_country() throws InterruptedException {
		Thread.sleep(2000);
		send_actions.select_country("Kenya"); //no country selection implementetion
	}
	@And("^user selects an wallet$")
	public void user_selects_an_wallet() {
		send_actions.select_wallet("mpesa");
	}
	@Then("^prompt user to enter wallet number$")
	public void prompt_user_to_enter_mobile_number() {
		send_actions.confirm_walletentry();
	}
	@Given("^user is on beneficiary details page$")
	public void user_is_on_beneficiary_details_page() {
	    // do nothing haha
	}

	@When("^user enters mobile wallet number$")
	public void user_enters_mobile_wallet_number() {
		send_actions.enter_walletno("722000000");
	}
	
}
