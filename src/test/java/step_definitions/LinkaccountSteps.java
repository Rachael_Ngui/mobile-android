package step_definitions;

import Actions.LinkaccountActions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LinkaccountSteps {
	/*
	 * @Given("^user is on homepage$") public void user_is_on_homepage() throws
	 * InterruptedException { //after user has logged in
	 * 
	 * LinkaccountActions link = new LinkaccountActions(); link.waiting();
	 * 
	 * }
	 */
	LinkaccountActions link_actions = new LinkaccountActions();
	@When("^user clicks link account$")
	public void user_clicks_link_account() {
		link_actions.link();
	}

	@Then("^proceed to link account process$")
	public void proceed_to_link_account_process() {
		System.out.println("link account process");
	}

}
