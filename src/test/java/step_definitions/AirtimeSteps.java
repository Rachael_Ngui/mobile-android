package step_definitions;

import Actions.AirtimeActions;
import Actions.SendmoneyActions;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;


public class AirtimeSteps {
	AirtimeActions airtime_actions = new AirtimeActions();
	
	@Before
	public void another() {
		System.out.println("just trying something out to see if it works");
	}
	@And("^user scrolls down$")
	public void user_scrolls_down() {
		 airtime_actions.scrolll();
	}
	
	@Then("^user should see buy airtime$")
	public void user_should_see_buy_airtime() {
		airtime_actions.confirm_airtime();
	}
	@When("^user clicks buy airtime$")
	public void user_clicks_buy_airtime() {
		airtime_actions.click_airtime();
	}
	@Then("^prompt user for source account$")
	public void prompt_user_for_source_account() {
	    // 
	}
	@When("^user clicks on the source account$")
	public void user_clicks_on_the_source_account() {
		airtime_actions.sourceaccount();
	}
	@When("^user clicks on beneficiary$")
	public void user_clicks_on_beneficiary() {
		airtime_actions.beneficiary();
	}
	@When("^user selects the mobile operator$")
	public void user_selects_the_mobile_operator() {
		airtime_actions.select_operator("Safaricom");
	}
	@Then("^prompt user to enter mobile number$")
	public void prompt_user_to_enter_mobile_number() {
		//
	}
	@When("^user enters recipient mobile number$")
	public void user_enters_recipient_mobile_number() {
		airtime_actions.enter_mobile("722000000");
	}
	
	@When("^user clicks buy airtime button$")
	public void user_clicks_buy_airtime_button() {
		airtime_actions.airtime_button();
	}
	@Then("^proceed to enter payment reason$")
	public void proceed_to_enter_payment_reason() {
		
		
	}

}
