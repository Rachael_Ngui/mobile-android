package step_definitions;

import Actions.PaybillActions;
import Actions.SendmoneyActions;
import cucumber.api.java.en.*;

public class PaybillSteps {
	PaybillActions paybill_actions = new PaybillActions();
	@Then("^user should see paybill functionality$")
	public void user_should_see_paybill_functionality() {
	    paybill_actions.confirm_paybill();
	}

	@When("^user clicks paybill$")
	public void user_clicks_paybill() {
		paybill_actions.click_paybill();
	}

	@When("^user clicks source account$")
	public void user_clicks_source_account() {
		paybill_actions.click_source();
	}

	@When("^user clicks select biller$")
	public void user_clicks_select_biller() {
		paybill_actions.click_biller();
	}
	
	@Then("^direct user to biller categories$")
	public void direct_user_to_biller_categories() {
		paybill_actions.confirm_billers();
	}
	@Given("^user is on billers page$")
	public void user_is_on_billers_page() throws InterruptedException {
	    Thread.sleep(15000);
	}

	@When("^user selects biller category$")
	public void user_selects_biller_category() {
		paybill_actions.biller_category("utilities");
	}

	@Then("^allow user to select biller$")
	public void allow_user_to_select_biller() throws InterruptedException {
		paybill_actions.billername("SGR");
	}

	@Given("^user is on biller account page$")
	public void user_is_on_biller_account_page() throws InterruptedException {
		Thread.sleep(10000);
		paybill_actions.confirm_billeraccount();
	}

	@When("^user enters biller account$")
	public void user_enters_biller_account() {
		paybill_actions.enter_billeraccount("1234567");
	}

}
