package step_definitions;
import java.io.IOException;
import java.net.MalformedURLException;

import Actions.*;
import DataCollector.LoginData;
import Profile.ConfigFileReader;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class LoginSteps {
	
	LoginActions login_actions = new LoginActions();
	@Before
	public String[] testData() throws IOException, InterruptedException {
		ConfigFileReader config = new ConfigFileReader();
		String sheet= config.getSheet();
		LoginData dt= new LoginData();
		String password =dt.login("password");;
		System.out.println("The configured sheet number is "+sheet);
		String[] data = {password};
		return data;
	}
			
	@Given ("^User is on landing page$")
	public void User_is_on_landing_page() throws MalformedURLException {
		LaunchApp.launchapp();
		//System.out.println("ssdjcdsjjf");
		
	}
	
	@When ("^User clicks login button$")
	public void User_clicks_login_button() {
		login_actions.loginbutton();
	}
	
	@Then ("User is prompted to enter login details$")
	public void User_is_prompted_to_enter_login_details() {
		System.out.println("enter your login details");
		
	}
	@Given("^User is already on landing page$")
	public void User_is_already_on_landing_page() throws Throwable {
		// allow user to remain on the login page
	}
	
	
	/*
	 * @When("^User enters incorrect password$") public void
	 * user_enters_wrong_password() throws Throwable { LoginActions lo = new
	 * LoginActions(); lo.password("test"); }
	 */
	 
	
	@And("^User clicks continue$")
	public void user_clicks_continue() throws Throwable {
		login_actions.continuebutton(); 
	}
	
	/*
	 * @Then("^Throw erro$") public void throw_error() { // throw wrong password
	 * error }
	 */
	
	@When("^User enters correct password$")
	public void User_enters_correct_password() throws InterruptedException, IOException {
		Thread.sleep(5000);
		String details[]=  testData();
		login_actions.password(details[0]);
	}
	
	@Then("^Login user$")
	public void login_user() {
		System.out.println("user logged in successfully");
	}

}
