package Scenarios;

import java.net.MalformedURLException;

import org.testng.annotations.Test;

import Actions.*;
 
public class LoginScenarios{
	LoginActions login = new LoginActions();
	@Test
	public void data(String[] credentials) throws InterruptedException, MalformedURLException {
		//login_button();
		//incorrect_mobile(credentials);
		//incorrect_email(credentials);
		//incorrect_password(credentials);
		//unsigned_user(credentials);
		incorrect_password(credentials);
		subsiquent_login(credentials);
	}
	/*
	public void login_button() {
		 login.loginbutton();
	}
	public void mobile_login(String[] credentials) throws InterruptedException {
		Thread.sleep(2000);
		//login.loginbutton();
		//login.username(credentials[3]);
		login.password(credentials[5]);
		login.continuebutton();
	}
	public void email_login(String[] credentials) throws InterruptedException{
		Thread.sleep(2000);
		login.loginbutton();
		login.username(credentials[0]);
		login.password(credentials[5]);
		login.continuebutton();
	}
	public void incorrect_mobile(String[] credentials) throws InterruptedException {
		login.username(credentials[4]);
		login.password(credentials[5]);
		login.continuebutton();
		Thread.sleep(2000);
		login.backbutton();
	}
	*/

	/*
	 * public void incorrect_email(String[] credentials) throws InterruptedException
	 * { Thread.sleep(2000); login.loginbutton(); login.username(credentials[1]);
	 * login.password(credentials[5]); login.continuebutton(); Thread.sleep(2000);
	 * login.backbutton(); }
	 */
	public void incorrect_password(String[] credentials) throws InterruptedException {
		Thread.sleep(2000);
		login.password(credentials[6]);
		login.continuebutton();
		
	}
	public void subsiquent_login(String[] credentials) throws InterruptedException {
		Thread.sleep(2000);
		login.password(credentials[5]);
		login.continuebutton();
	}
	/*
	 * public void unsigned_user(String[] credentials) throws InterruptedException {
	 * Thread.sleep(2000); login.loginbutton(); login.username(credentials[1]);
	 * login.password(credentials[6]); login.continuebutton(); Thread.sleep(2000);
	 * login.backbutton();
	 * 
	 * }
	 */

}
