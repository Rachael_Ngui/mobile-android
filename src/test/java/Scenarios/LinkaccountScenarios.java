package Scenarios;

import Actions.LinkaccountActions;

public class LinkaccountScenarios {
	LinkaccountActions link = new LinkaccountActions();
	public void data(String country,String idtype,String idnumber) throws InterruptedException {
		linkbutton();
		addinfo(country,idtype,idnumber);
	}
	public void linkbutton() throws InterruptedException {
		Thread.sleep(25000);
		link.link();
		Thread.sleep(2000);
		link.next();
	}
	public void editfirstname() {
		
	}
	
	public void editlastname() {
		
	}
	public void editdob() {
		
	}
	public void addinfo(String country,String idtype,String idnumber) {
		link.country();
		link.selectcountry(country);
		link.id_type();
		link.selectid_type(idtype);
		link.id_number(idnumber);
		link.next();
	}

}
