package DataCollector;

import java.io.IOException;
import java.net.MalformedURLException;


import org.testng.annotations.BeforeTest;

import Actions.HomepageActions;
import Actions.LaunchApp;
import Actions.SignUp;
import Scenarios.LoginScenarios;

import org.testng.annotations.*;


public class Control {
	
	@Test(priority=1,enabled=true,description="Launch eazzy app")
	public void launch() throws MalformedURLException {
		LaunchApp.launchapp();	
	}
	@Test(priority=2,enabled=true,description="Login into the app")
	public void login() throws IOException, InterruptedException {
		LoginData ld = new LoginData();
		ld.login(null);
	}
	@Test(priority=3,enabled=false,description="Login into the app")
	public void homepage() throws IOException, InterruptedException {
		HomepageActions home = new HomepageActions();
		home.linkaccount();
	}
	
	
}
