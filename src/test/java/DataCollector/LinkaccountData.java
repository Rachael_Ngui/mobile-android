package DataCollector;

import java.io.IOException;

import Scenarios.LinkaccountScenarios;

public class LinkaccountData {
	DataReader reader = new DataReader();
	LinkaccountScenarios scenario = new LinkaccountScenarios();
	
	public void linkdata() throws IOException, InterruptedException {
		String country= reader.read(0, 10, 1);
		String idtype= reader.read(0, 11, 1);
		String idnumber= reader.read(0, 12, 1);
		
		scenario.data(country, idtype, idnumber);
	}

}
