package DataCollector;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataReader {
	public String read(int sheet_no,int row,int column) throws IOException {
		
		String value= null;          //variable for storing the cell value  
		Workbook wb=null;  
		try {
			FileInputStream fis=new FileInputStream("/home/rachael/eclipse-workspace/OneEquity/testdata/testdata.xlsx"); 
			wb=new XSSFWorkbook(fis);  
			
		}catch(FileNotFoundException e) {
			e.printStackTrace();  
		}
		Sheet sheet=wb.getSheetAt(sheet_no);   	//getting the XSSFSheet object at given index  
		Row vRow=sheet.getRow(row);    		 //returns the logical row  
		Cell cell=vRow.getCell(column); 	//getting the cell representing the given column  
		value=cell.getStringCellValue();
		
		return value;
		
		
		
	}

}
