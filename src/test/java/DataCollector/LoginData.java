package DataCollector;

import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.annotations.Test;

import Actions.LaunchApp;
import Scenarios.LoginScenarios;

public class LoginData {
	DataReader data = new DataReader();
	//LoginScenarios scenarios = new LoginScenarios();
	public String login(String credential) throws IOException, InterruptedException {
		String email= data.read(0, 1, 1);
		String invalid_email= data.read(0, 2, 1);
		String wrong_email= data.read(0, 3, 1);
		String mobile= data.read(0, 4, 1);
		String wrong_mobile= data.read(0, 6, 1);
		String password= data.read(0, 7, 1);
		String wrong_password= data.read(0, 8, 1);
		String credentials[]={email,invalid_email,wrong_email,mobile,wrong_mobile,password,wrong_password};
		
		if (credential.contains("password")) {
			credential=password;
		}
		else if(credential.contains("mobile")) {
			credential=mobile;
		}
		else {
			System.out.println("Please specify the credential needed");
		}
		
		return credential;
		
	}
	
	}


