package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class OpenaccountObjects {
	public OpenaccountObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
//BUTTONS
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/applyNowButton")
	public AndroidElement applynow;
	
// REVIEW YOUR DETAILS OBJECTS
	// name
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/nameValueEdit")
	public AndroidElement editnameicon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/nameValue")
	public AndroidElement namevalue;
	
	// DOB
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/dateOfBirthValueEdit")
	public AndroidElement editDOBicon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/dateOfBirthValue")
	public AndroidElement DOBvalue;
	
	//country of residence
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/countryValueEdit")
	public AndroidElement editcountryicon;
	
    @FindBy(id="ke.co.equitybank.oneequity.debug:id/countryValue") public
	AndroidElement countryvalue;
	 
	//ID type
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/idTypeValueEdit")
	public AndroidElement editIDtypeicon;
		
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/idTypeValue")
	public AndroidElement IDtypevalue;
	
	// ID number
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/idNumberValueEdit")
	public AndroidElement editIDnumbericon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/idNumberValue")
	public AndroidElement IDnumbervalue;
	
	// next button
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/nextButton")
	public AndroidElement next;
	
//TERMS OF SERVICE
	// terms and conditions
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/iAcceptRadioButton")
	public AndroidElement terms;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/iAcceptButton")
	public AndroidElement acceptterms;

}
