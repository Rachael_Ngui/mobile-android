package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendmoneyObjects {
	
	public SendmoneyObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(xpath="//android.widget.ImageButton[@index='0']")
	public AndroidElement Xbutton;

// COMMON IN SEND MONEY FUNCTIONALITIES
	
	@FindBy(xpath="//android.widget.TextView[@text='Choose origin account']")
	public AndroidElement sourceaccount;
	
	@FindBy(xpath="//android.view.ViewGroup[@index='0']")
	public AndroidElement accountone;
	
	@FindBy(xpath="//android.view.ViewGroup[@index='1']")
	public AndroidElement accounttwo;
	
	@FindBy(xpath="//android.widget.TextView[@text='Choose or enter your contact']")
	public AndroidElement destinationaccount;
	
	@FindBy(xpath="//androidx.cardview.widget.CardView[@index='1']")
	public AndroidElement accountlist;
	
	@FindBy(xpath="//android.widget.TextView[@text='Review']")
	public AndroidElement reviewdetails;
	
 // SEND TO SOMEONE
	
	@FindBy(xpath="//android.widget.TextView[@text='Mobile wallet']")
	public AndroidElement mobilewallet;
	
	@FindBy(xpath="//android.widget.TextView[@text='Equity account']")
	public AndroidElement toequity;
	
	@FindBy(xpath="//android.widget.TextView[@text='Another bank account']")
	public AndroidElement otherbankaccount;
	
	@FindBy(xpath="//android.widget.TextView[@text='Phone-linked account']")
	public AndroidElement otherbankphone;
	
	@FindBy(xpath="//android.widget.TextView[@text='PayPal']")
	public AndroidElement paypal;
	
	@FindBy(xpath="//android.widget.TextView[@text='Cash Code']")
	public AndroidElement cashcode;
	
 // SEND TO EQUITY ACCOUNT
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/accountOrPhoneNumberValue")
	public AndroidElement beneficiaryaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/addBeneficiary")
	public AndroidElement addbutton;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toSomeoneNew")
	public AndroidElement tosomeonenew;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payToParent")
	public AndroidElement recipient;
	
 // SEND TO WALLETS
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/header")
	public AndroidElement operatorstab;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/itemText")
	public AndroidElement operatorcountry;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/enterBeneficiaryDetailsTitle")
	public AndroidElement beneficiarydetails;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/phoneNumberValue")
	public AndroidElement walletno;
	
	
	
 // SEND TO OTHER BANK
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toChargeOptions")
	public AndroidElement chargeoptions;
	
	@FindBy(xpath="//android.widget.TextView[@text='Our']")
	public AndroidElement our;
	
	@FindBy(xpath="//android.widget.TextView[@text='Beneficiary']")
	public AndroidElement beneficiary;
	
	@FindBy(xpath="//android.widget.TextView[@text='Shared']")
	public AndroidElement shared;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toSupportingDocumentsToggle")
	public AndroidElement supportingdoc;
	
 // CONFIRM TRANSACTION DETAILS
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/fromValue")
	public AndroidElement fromdetails;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toValueSubText")
	public AndroidElement todetails;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toValue")
	public AndroidElement tovalue;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/amountValue")
	public AndroidElement amountdetails;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/feesValue")
	public AndroidElement chargedetails;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/thankFeedbackTitle")
	public AndroidElement success;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/backToTransactionButton")
	public AndroidElement donebutton;
	
	
}
