package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LinkaccountObjects {
	public LinkaccountObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	  // confirm customer details (next button)
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/nextButton")
	public AndroidElement nextbutton;
	
	// select country object
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/chooseCountryDisplay")
	public AndroidElement country;
	
	// ID type selection objects
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/idTypeValue")
	public AndroidElement IDtype;
	
	// Enter ID objects
	@FindBy(xpath="//android.widget.EditText[@text='ID number']")
	public AndroidElement IDnumber;

}
