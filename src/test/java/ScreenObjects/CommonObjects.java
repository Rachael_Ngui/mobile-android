package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CommonObjects {
	public CommonObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
// LIST OF COUNTRIES
	
	@FindBy(xpath="//android.widget.TextView[@text='Kenya']")
	public AndroidElement kenya;
	
	@FindBy(xpath="//android.widget.TextView[@text='Uganda']")
	public AndroidElement uganda;
	
	@FindBy(xpath="//android.widget.TextView[@text='Tanzania']")
	public AndroidElement tanzania;
	
	@FindBy(xpath="//android.widget.TextView[@text='South Sudan']")
	public AndroidElement southsudan;
	
	@FindBy(xpath="//android.widget.TextView[@text='Rwanda']")
	public AndroidElement rwanda;
	
	@FindBy(xpath="//android.widget.TextView[@text='Democratic Republic of the Congo']")
	public AndroidElement drc; 
	
// LIST OF ID TYPES
	
	@FindBy(xpath="//android.widget.TextView[@text='Alien ID']")
	public AndroidElement alienID;
	
	@FindBy(xpath="//android.widget.TextView[@text='Driving license']")
	public AndroidElement drivinglicense;
	
	@FindBy(xpath="//android.widget.TextView[@text='Military ID']")
	public AndroidElement militaryID;
	
	@FindBy(xpath="//android.widget.TextView[@text='National ID']")
	public AndroidElement nationalID;
	
	@FindBy(xpath="//android.widget.TextView[@text='Passport']")
	public AndroidElement passport;
	
	@FindBy(xpath="//android.widget.TextView[@text='Voters card']")
	public AndroidElement voterscard;
	
// CURRENCIES and AMOUNT
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/currencyClick")
	public AndroidElement choosecurrency;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/editTextValue")
	public AndroidElement amount;
	
	@FindBy(xpath="//android.widget.TextView[@text='EUR']")
	public AndroidElement EUR;
	
	@FindBy(xpath="//android.widget.TextView[@text='GBP']")
	public AndroidElement GBP;
	
	@FindBy(xpath="//android.widget.TextView[@text='KES']")
	public AndroidElement KES;
	
	@FindBy(xpath="//android.widget.TextView[@text='USD']")
	public AndroidElement USD;
	
	@FindBy(xpath="//android.widget.TextView[@text='ZAR']")
	public AndroidElement ZAR;
	
 // PAYMENT REASON
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/paymentReasonValue")
	public AndroidElement paymentreason;
	
 // BUTTONS
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/sendAction")
	public AndroidElement sendbutton;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payButton")
	public AndroidElement paybutton;
	
	@FindBy(xpath="//android.widget.ImageButton[@index='0']")
	public AndroidElement Xbutton;
	
 // LIST OF MOBILE WALLETS
	
	@FindBy(xpath="//android.widget.TextView[@text='MPesa']")
	public AndroidElement mpesa;
	
	@FindBy(xpath="//android.widget.TextView[@text='Airtel Money']")
	public AndroidElement airtelmoney;
	
 // ERROR MESSAGES
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/errorText")
	public AndroidElement errorMessage;
	
}
