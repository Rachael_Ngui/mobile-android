package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginObjects {
	
	
	public LoginObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements( new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/loginAction")
	public AndroidElement Login;
	
	@FindBy(xpath="//android.widget.ImageButton[@index='0']")
	public AndroidElement backbutton;
	
	@FindBy(xpath="//android.widget.EditText[@text='One Equity email or phone number']")
	public AndroidElement username;
	
	@FindBy(xpath="//android.widget.EditText[@text='Enter password']")
	public AndroidElement password;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/forgotPasswordButton")
	public AndroidElement forgotpass;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/letMeIn")
	public AndroidElement continuebutton;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/usePin")
	public AndroidElement usepin;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/useFingerPrint")
	public AndroidElement fingerprint;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/text_input_end_icon")
	public AndroidElement showpass;

}
