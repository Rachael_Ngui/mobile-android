package ScreenObjects;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PaybillObjects {
	public PaybillObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/itemPayFromBalance")
	public AndroidElement fromaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payToBiller")
	public AndroidElement selectbiller;

 // Billers
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/billerSearchEditText")
	public AndroidElement billersearch;
	
	@FindBy(xpath="//android.widget.TextView[@text='Utilities']")
	public AndroidElement utilities;
	
	@FindBy(xpath="//android.widget.TextView[@text='Schools and University']")
	public AndroidElement schools;
	
	@FindBy(xpath="//android.widget.TextView[@text='Churches']")
	 public AndroidElement churches;
	
	@FindBy(xpath="//android.widget.TextView[@text='Government And Tax Payments']")
	public AndroidElement government;
	
	@FindBy(xpath="//android.widget.TextView[@text='E-Commerce And Online Payments']")
	public AndroidElement ecommerce;
	
	@FindBy(xpath="//android.widget.TextView[@text='Microfinance and Saccos']")
	public AndroidElement saccos;
	
	@FindBy(xpath="//android.widget.TextView[@text='FMCG and Distributors']")
	public AndroidElement distributors;
	
	@FindBy(xpath="//android.widget.TextView[@text='Others']")
	public AndroidElement others;
	
 // Lists of Billers
	
	@FindBy(xpath="//android.widget.TextView[@index='0']")
	public List<AndroidElement> billerslist;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/billersList")
	public List<AndroidElement> scrollbiller;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payBillAccountNumber")
	public AndroidElement accounttext;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/edittext")
	public AndroidElement accountfield;
}
