package ScreenObjects;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomepageObjects {
	public HomepageObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
 // ELEMENTS UNDER HOME ICON
	
	@FindBy(xpath="//android.widget.TextView[@index='0']")
	public AndroidElement notification;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/linkAccountNewUser")
	public AndroidElement linkaccountnew;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/linkAccount")
	public AndroidElement linkaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/openAccountNewUser")
	public AndroidElement openaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/accountNumber")
	public List<AndroidElement> linkedaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/accountList")
	public AndroidElement accountcontainer;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/insuranceOffer")
	public AndroidElement insurance;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/stocksOffer")
	public AndroidElement stock;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/lifestyleOffer")
	public AndroidElement lifestyle;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/rewardsOffer")
	public AndroidElement rewards;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/homeFragment")
	public AndroidElement homeicon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/transfer_landing_nav_graph")
	public AndroidElement transfericon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/lifeStyle")
	public AndroidElement lifestyleicon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/invest")
	public AndroidElement investicon;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/profileHostFragment")
	public AndroidElement moreicon;
	
 // ELEMENTS UNDER TRANSFER ICON
	
	@FindBy(xpath="//android.widget.TextView[@text='Send money to someone']")
	public AndroidElement sendtosomeone;
	
	@FindBy(xpath="//android.widget.TextView[@text='Send money to your own account']")
	public AndroidElement sendtoown;
	
	@FindBy(xpath="//android.widget.TextView[@text='Receive money']")
	public AndroidElement receivemoney;
	
	@FindBy(xpath="//android.widget.TextView[@text='Buy airtime']")
	public AndroidElement buyairtime;
	
	@FindBy(xpath="//android.widget.TextView[@text='Pay bills']")
	public AndroidElement paybill;
	
	@FindBy(xpath="//android.widget.TextView[@text='Buy goods']")
	public AndroidElement buygoods;
	
	@FindBy(xpath="//android.widget.TextView[@text='Withdraw']")
	public AndroidElement withdraw;
	
	@FindBy(xpath="//android.widget.ScrollView[@index='1']")
	public AndroidElement transferscrollable;

}

