package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyairtimeObjects {
	public BuyairtimeObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payFromParent")
	public AndroidElement source;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payToParent")
	public AndroidElement beneficiary;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/chooseContactSelection")
	public AndroidElement contactselection;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toSomeoneNew")
	public AndroidElement tosomeonenew;
	
	// Mobile Operators
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/countryPicker")
	public AndroidElement countrypicker;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/operatorsList")
	public AndroidElement operatorlist;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/itemText")
	public AndroidElement safaricom;
	
	@FindBy(xpath="//android.widget.TextView[@text='Airtel']")
	public AndroidElement airtel;
	
	@FindBy(xpath="//android.widget.TextView[@text='Equitel']")
	public AndroidElement equitel;
	
	@FindBy(xpath="//android.widget.TextView[@text='Telkom']")
	public AndroidElement telkom;
	
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/phoneNumberValue")
	public AndroidElement mobilenumber;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/payButton")
	public AndroidElement buyairtimeButton;
	

}
