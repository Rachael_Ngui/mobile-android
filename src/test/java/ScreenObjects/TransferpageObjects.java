package ScreenObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TransferpageObjects {
	
	public TransferpageObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toSendToSomeone")
	public AndroidElement sendtosomeone;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toSendToOwnAccount")
	public AndroidElement sendtoownaccount;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toReceiveMoney")
	public AndroidElement receivemoney;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toBuyAirtime")
	public AndroidElement buyairtime;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toPayBills")
	public AndroidElement paybills;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toBuyGoods")
	public AndroidElement buygoods;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/toWithdraw")
	public AndroidElement withdraw;

}
