package Actions;

import java.net.MalformedURLException;

import org.testng.annotations.Test;

import Utilities.DriverFactory;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SignUp {
	
	static AndroidDriver<AndroidElement> driver;// = new AndroidDriver<AndroidElement>(null);
	
	public static void signup() throws MalformedURLException {
		driver=DriverFactory.driverlaunch();
		AndroidElement sign_up =driver.findElementById("ke.co.equitybank.oneequity.debug:id/signUpAction");
		sign_up.click();
	}
	public static void user_details() throws MalformedURLException {
		driver=DriverFactory.driverlaunch();
		AndroidElement firstname = driver.findElementByXPath("//android.widget.EditText[@text='First name']");
		AndroidElement lastname = driver.findElementByXPath("//android.widget.EditText[@text='Last name']");
		AndroidElement mobile = driver.findElementByXPath("//android.widget.EditText[@text='Mobile number']");
		AndroidElement DOB = driver.findElementByXPath("//android.widget.EditText[@text='Date of birth']");
		AndroidElement email = driver.findElementByXPath("//android.widget.EditText[@text='Email address']");
		AndroidElement createprofile = driver.findElementById("ke.co.equitybank.oneequity.debug:id/continueButton");
		
		firstname.sendKeys("recheal");
		lastname.sendKeys("kathini");
		mobile.sendKeys("719695047");
		DOB.sendKeys("23/08/1996");
		email.sendKeys("kathinirachael3@gymail.com");
		createprofile.click();
		
	}
	public static void terms() throws MalformedURLException {
		driver=DriverFactory.driverlaunch();
		AndroidElement terms= driver.findElementById("ke.co.equitybank.oneequity.debug:id/webview");
		AndroidElement accept= driver.findElementById("ke.co.equitybank.oneequity.debug:id/acceptButton");
		
		terms.click();
		accept.click();
		
	}
	public static void password() throws MalformedURLException {
	    driver=  DriverFactory.driverlaunch();
		AndroidElement password= driver.findElementByXPath("//android.widget.EditText[@text='Password']");
		AndroidElement confirmpass= driver.findElementByXPath("//android.widget.EditText[@text='Confirm password']");
		AndroidElement confirmbutton= driver.findElementById("ke.co.equitybank.oneequity.debug:id/confirmButton");
		
		password.sendKeys("test123");
		confirmpass.sendKeys("test123");
		confirmbutton.click();
	}

}
