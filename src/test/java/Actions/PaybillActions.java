package Actions;

import java.time.Duration;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.Dimension;

import ScreenObjects.HomepageObjects;
import ScreenObjects.PaybillObjects;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class PaybillActions extends LaunchApp{
	
	HomepageObjects home = new HomepageObjects(driver);
	PaybillObjects paybill= new PaybillObjects(driver);
	
	public void confirm_paybill() {
		//driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).scrollIntoView(text(\"Withdraw\"));");
		Assert.assertTrue(home.paybill.getText().contains("Pay bills"));
	}
	public void click_paybill() {
		home.paybill.click();
	}
	public void click_source() {
		paybill.fromaccount.click();
	}
	public void click_biller() {
		paybill.selectbiller.click();
	}
	public void confirm_billers() {
		Assert.assertTrue(paybill.billersearch.getText().contains("Search"));
	}
	public void biller_category(String category) {
		if(category.contains("utilities")) {
			paybill.utilities.click();
		}
		else if(category.contains("schools")) {
			paybill.schools.click();
		}
		else if(category.contains("churches")) {
			paybill.churches.click();
		}
		else if(category.contains("government")) {
			paybill.government.click();
		}
		else if(category.contains("ecommerce")) {
			paybill.ecommerce.click();
		}
		else if(category.contains("saccos")) {
			paybill.saccos.click();
		}
		else if(category.contains("distributors")) {
			paybill.distributors.click();
		}
		else if(category.contains("others")) {
			paybill.others.click();
		}
		else {
			System.out.println("Please specify a biller");
		}
	}
	
	public void billername(String billername) throws InterruptedException {
		Thread.sleep(2000);
		List<AndroidElement> billers = paybill.billerslist;
		for(AndroidElement biller : billers) {
			
			if (biller.getText().contains(billername)) {
				System.out.println(biller.getText());
				biller.click();
				break;
			}
			
		}
		
	}
	
	public void scroll() {
		
		Dimension dimension= driver.manage().window().getSize();
		Double startheight = dimension.getHeight() * 0.5 ;
		int scrollstart= startheight.intValue();
		
		Double endheight = dimension.getHeight() * 0.2 ;
		int scrollend= endheight.intValue();
		
		TouchAction touch = new TouchAction(driver);
		touch.press(PointOption.point(0,scrollstart)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
		.moveTo(PointOption.point(0, scrollend)).release().perform();
		
	
		
		///driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"ke.co.equitybank.oneequity.debug:id/billersList\")).scrollIntoView(new UiSelector().textContains(\"KPLC TOKENS\"));").click();
		System.out.println("scrolled");
	}
	
	public void confirm_billeraccount() {
		Assert.assertTrue(paybill.accounttext.getText().contains("Please enter the following fields"));
	}
	public void enter_billeraccount(String billeraccount) {
		paybill.accountfield.sendKeys(billeraccount);
	}

}
