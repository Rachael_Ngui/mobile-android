package Actions;

import java.time.Duration;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ScreenObjects.CommonObjects;
import ScreenObjects.HomepageObjects;
import ScreenObjects.SendmoneyObjects;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class SendmoneyActions extends LaunchApp{
	
	SendmoneyObjects send = new SendmoneyObjects(driver);
	HomepageObjects home= new HomepageObjects(driver);
	CommonObjects common = new CommonObjects(driver);
	
	public void click_transfer() {
		home.transfericon.click();
	}
	public void findsendtoown() {
		Assert.assertTrue(home.sendtoown.getText().contains("Send money to own accounts"));
		
	}
	public void click_sendtoown() {
		home.sendtoown.click();	
	}
	public void confirm_sourceaccount() {
		Assert.assertTrue(send.sourceaccount.getText().contains("Choose origin account"));
	}
	public void click_source() {
		send.sourceaccount.click();
	}
	public void select_source() {
		send.accountlist.click();
	}
	public void click_destination() {
		send.destinationaccount.click();
	}
	public void select_dest() {
		send.accountlist.click();
	}
	public void enter_dest() throws InterruptedException {
		send.destinationaccount.click();
		Thread.sleep(3000);
		send.accountlist.click();
	}
	public void enter_amount(String amount) {
		common.amount.click();
		common.amount.sendKeys(amount);
	}
	public void click_send() {
		common.sendbutton.click();
	}
	public void reviewdetails() {
		Assert.assertTrue(send.reviewdetails.getText().contains("Review"));
	}
	public void check_from() {
		Assert.assertTrue(send.fromdetails.getText()!= null);
	}
	public void check_to() {
		Assert.assertTrue(send.tovalue.getText()!= null);
	}
	public void check_amount() {
		Assert.assertTrue(send.amountdetails.getText()!= null);
	}
	public void check_charge() {
		Assert.assertTrue(send.chargedetails.getText()!= null);
	}
	public void click_pay() {
		common.paybutton.click();
	}
	public void success() {
		Assert.assertTrue(send.success.getText().contains("Great!"));
	}
	public void click_done() {
		send.donebutton.click();
	}
	public void click_currency() {
		common.choosecurrency.click();
	}
	public void select_currency(String currency) {
		if(currency.contains("KES")) {
			common.KES.click();
		}
		else if(currency.contains("UGX")) {
			common.KES.click();
		}
		else if(currency.contains("TZS")) {
			common.KES.click();
		}
		else if(currency.contains("RWF")) {
			common.KES.click();
		}
		else if(currency.contains("SSP")) {
			common.KES.click();
		}
		else if(currency.contains("USD")) {
			common.USD.click();
		}
		else {
			System.out.println("please specify currency");
		}
		
	}
	public void click_cancel() {
		common.Xbutton.click();
	}
	public void confirm_errormessage(String error) {
		Assert.assertTrue(common.errorMessage.getText().contains(error));
	}
	public void send_inactive() {
		Assert.assertTrue(common.sendbutton.getAttribute("enabled").contains("false"));
	}
	public void click_sendtosomeone() {
		home.sendtosomeone.click();
	}
	public void confirm_sendtoequity() {
		Assert.assertTrue(send.toequity.getText().contains("Equity"));
	}
	public void click_sendtoequity() {
		send.toequity.click();
	}
	public void click_someonenew() {
		send.tosomeonenew.click();
	}
	public void click_recipient() {
		send.recipient.click();
	}
	public void enter_beneficiary(String beneficiary) {
		send.beneficiaryaccount.sendKeys(beneficiary);
	}
	public void click_add() {
		send.addbutton.click();
	}
	public void payment_reason(String reason) {
		common.paymentreason.click();
		common.paymentreason.sendKeys(reason);
	}
	public void confirm_wallets() {
		Assert.assertTrue(send.mobilewallet.getText().contains("Mobile wallet"));
	}
	public void click_wallets() {
		send.mobilewallet.click();
	}
	public void confirm_operator() {
		Assert.assertTrue(send.operatorstab.getText().contains("Please select the operator"));
	}
	public void operator_country() {
		send.operatorcountry.click();
	}
	public void select_country(String country) {
		if(country.contains("Kenya")) {
			common.kenya.click();
		}
		else if(country.contains("Uganda")) {
			common.uganda.click();
		}
		else if(country.contains("Tanzania")) {
			common.tanzania.click();
		}
		else if(country.contains("Rwanda")) {
			common.rwanda.click();
		}
		else if(country.contains("S.sudan")) {
			common.southsudan.click();
		}
		else if(country.contains("DRC")) {
			common.drc.click();
		}
		else {
			System.out.println("Please specify the country");
		}
	}
	public void select_wallet(String wallet) {
		if(wallet.contains("mpesa")) {
			common.mpesa.click();
		}
		else if (wallet.contains("airtel money")) {
			common.airtelmoney.click();
		}
		else {
			System.out.println("Please specifiy the wallet");
		}
		
	}
	public void confirm_walletentry() {
		Assert.assertTrue(send.beneficiarydetails.getText().contains("Enter beneficiary details"));
	}
	public void enter_walletno(String walletno) {
		send.walletno.sendKeys(walletno);
	}
	/*
	 * public void explicit_wait() { FluentWait<AndroidDriver<AndroidElement>> wait
	 * = new FluentWait<AndroidDriver<AndroidElement>>(driver)
	 * .withTimeout(Duration.ofSeconds(20)) .pollingEvery(Duration.ofSeconds(2))
	 * .ignoring(Exception.class); wait.until();
	 * 
	 * }
	 */

}
