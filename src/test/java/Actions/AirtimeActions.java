package Actions;

import org.junit.Assert;

import ScreenObjects.BuyairtimeObjects;
import ScreenObjects.CommonObjects;
import ScreenObjects.HomepageObjects;

public class AirtimeActions extends LaunchApp{
	HomepageObjects home= new HomepageObjects(driver);
	CommonObjects common = new CommonObjects(driver);
	BuyairtimeObjects airtime = new BuyairtimeObjects(driver);
	public void scrolll() {
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(text(\"Buy airtime\"));");
	}
	public void confirm_airtime() {
		Assert.assertTrue(home.buyairtime.getText().contains("Buy airtime"));
	}
	public void click_airtime() {
		home.buyairtime.click();
	}
	public void select_operator(String operator) {
		if (operator.contains("Safaricom")) {
			airtime.safaricom.click();
		}
		else if (operator.contains("Airtel")) {
			airtime.airtel.click();
		}
		else if (operator.contains("Equitel")) {
			airtime.equitel.click();
		}
		else if (operator.contains("Telkom")) {
			airtime.telkom.click();
		}
		else {
			System.out.println("Please specify a mobile operator");
		}
	}
	public void sourceaccount() {
		airtime.source.click();
	}
	public void beneficiary() {
		airtime.beneficiary.click();
	}
	public void enter_mobile(String  mobile) {
		airtime.mobilenumber.sendKeys(mobile);
	}
	public void airtime_button() {
		airtime.buyairtimeButton.click();
	}

}
