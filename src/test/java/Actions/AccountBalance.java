package Actions;

import java.util.List;

import ScreenObjects.HomepageObjects;
import io.appium.java_client.android.AndroidElement;

public class AccountBalance extends LaunchApp{

	HomepageObjects home = new HomepageObjects(driver);
	
	public void click_account() throws InterruptedException {
		Thread.sleep(10000);
		List<AndroidElement> accounts =	home.linkedaccount;
		System.out.println(accounts.size());
		for(AndroidElement account: accounts) {
			if(account.getText().contains("Savings")) {
				account.click();
				break;
			}
		}
	}
}
