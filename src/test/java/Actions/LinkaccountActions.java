package Actions;

import ScreenObjects.CommonObjects;
import ScreenObjects.HomepageObjects;
import ScreenObjects.LinkaccountObjects;

public class LinkaccountActions extends LaunchApp{
	HomepageObjects home= new HomepageObjects(driver);
	LinkaccountObjects linkobj= new LinkaccountObjects(driver);
	CommonObjects common = new CommonObjects(driver);
	public void linknew() {
		home.linkaccountnew.click();
	}
	public void link() {
		home.linkaccount.click();
	}
	public void next() {
		linkobj.nextbutton.click();
	}
	public void country() {
		linkobj.country.click();
	}
	public void selectcountry(String country) {
		//linkobj.country.click();
		
		if(country.contains("Kenya") || country.contains("kenya")) {
			common.kenya.click();
		}
		else if (country.contains("uganda") || country.contains("Uganda")) {
			common.uganda.click();
		}
		else if (country.contains("south sudan") || country.contains("South sudan")) {
			common.southsudan.click();	
		}
		else if (country.contains("tanzania") || country.contains("Tanzania")) {
			common.tanzania.click();
		}
		else if (country.contains("rwanda") || country.contains("Rwanda")) {
			common.rwanda.click();
		}
		else if(country.contains("DRC") || country.contains("drc")) {
			common.drc.click();
		}
		else {
			System.out.println("The provided Country is not recognised");
		}
		
	}
	public void id_type() {
		linkobj.IDtype.click();
	}
	public void selectid_type(String idtype) {
		//linkobj.IDtype.click();
		
		if(idtype.contains("Alien ID")) {
			common.alienID.click();
		}
		else if (idtype.contains("National ID")) {
			common.nationalID.click();
		}
		else if (idtype.contains("Military ID")) {
			common.militaryID.click();
		}
		else if (idtype.contains("Passport")) {
			common.passport.click();
		}
		else if (idtype.contains("voters card")) {
			common.voterscard.click();
		}
		else {
			System.out.println("The provided ID type is not recognised");
		}
		
	}
	public void id_number(String id) {
		linkobj.IDnumber.sendKeys(id);
	}
	
	public void waiting() throws InterruptedException {
		Thread.sleep(10000);
	}

}
