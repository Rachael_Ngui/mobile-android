package Actions;

import ScreenObjects.HomepageObjects;
import ScreenObjects.OpenaccountObjects;

public class OpenaccountActions extends LaunchApp{
	HomepageObjects home = new HomepageObjects(driver);
	OpenaccountObjects open = new OpenaccountObjects(driver);
	public void openaccount() {
		home.openaccount.click();
	}
	public void applynow() {
		open.applynow.click();
	}
}
