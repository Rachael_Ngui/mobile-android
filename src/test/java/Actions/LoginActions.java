package Actions;

import ScreenObjects.LoginObjects;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LoginActions extends LaunchApp{
	
	//static AndroidDriver<AndroidElement> driver;// = new AndroidDriver<AndroidElement>(null);
	 LoginObjects loginobject= new LoginObjects(driver);
	 
	 
	public void loginbutton() { 
		loginobject.Login.click();
	}
	public void username(String username) throws InterruptedException {
		Thread.sleep(2000);
		loginobject.username.sendKeys(username);
	}
	public void password(String password) {
		loginobject.password.sendKeys(password);
	}
	public void forgotpass() {
		loginobject.forgotpass.click();
	}
	public void continuebutton() {
		loginobject.continuebutton.click();
	}
	public void backbutton() {
		loginobject.backbutton.click();
	}
	public void showpass() throws InterruptedException {
		loginobject.showpass.click();
	}

}
