package Profile;

import java.net.MalformedURLException;

import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LaunchApp {
	
	static AndroidDriver<AndroidElement> driver ;
	@Test
	public static void launchapp() throws MalformedURLException {
		driver=DriverFactory.driverlaunch();
		AndroidElement app =driver.findElementByXPath("//android.widget.TextView[@text='EazzyBankingV2']");
		app.click();
	}


}
