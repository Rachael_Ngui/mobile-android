package Profile;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginObjects {
	
	
	public LoginObjects(AndroidDriver<AndroidElement> driver) {
		PageFactory.initElements( new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/loginAction")
	AndroidElement Login;
	
	@FindBy(xpath="//android.widget.EditText[@text='One Equity email or phone number']")
	AndroidElement username;
	
	@FindBy(xpath="//android.widget.EditText[@text='Password']")
	AndroidElement password;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/forgotPasswordButton")
	AndroidElement forgotpass;
	
	@FindBy(id="ke.co.equitybank.oneequity.debug:id/loginButton")
	AndroidElement proceed;

}
