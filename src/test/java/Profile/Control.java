package Profile;

import java.net.MalformedURLException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Control {
	
	
	@Test(priority=1,enabled=true,description="Launch eazzy app")
	public void launch() throws MalformedURLException {
		LaunchApp.launchapp();
		
	}
	@Test(priority=2,enabled=false,description="Sign up function")
	public void sign() throws MalformedURLException {
		SignUp.signup();
	}
	
	@Test(priority=3,enabled=false,description="Provide user details")
	public void user() throws MalformedURLException {
		SignUp.user_details();
	}
	@Test(priority=4,enabled=false,description="Accept terms and conditions")
	public void terms() throws MalformedURLException {
		SignUp.terms();
	}
	@Test(priority=5,enabled=false,description="Set a password")
	public void password() throws MalformedURLException {
		SignUp.password();
	}
	@Test(priority=6,enabled=true,description="Set a password")
	public void login() throws MalformedURLException, InterruptedException {
		LoginActions log = new LoginActions();
		log.login();
		log.userdetails();
		log.submit();
	}

}
