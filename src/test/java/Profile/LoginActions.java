package Profile;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class LoginActions {
	
	static AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(null);
	 LoginObjects lo= new LoginObjects(driver);
	public void login() {
		lo.Login.click();
	}
	public void userdetails() {
		lo.username.sendKeys("+254719695047");
		lo.password.sendKeys("test123");
	}
	public void forgotpass() {
		lo.forgotpass.click();
	}
	public void submit() {
		lo.proceed.click();
	}

}
