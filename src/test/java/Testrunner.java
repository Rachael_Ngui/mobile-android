import org.junit.runner.RunWith;

import Profile.ConfigFileReader;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@RunWith(Cucumber.class)

  @CucumberOptions( features= "features", 
  glue="step_definitions", 
  //tags={"@login,@toOwn"},
  plugin = { "pretty","html:target/cucumber-reports" } )
 
 public class Testrunner extends AbstractTestNGCucumberTests{
	/*
	  ConfigFileReader configuration = new ConfigFileReader(); 
	  public Testrunner(){
		  String features=configuration.getFeatures();
		  String glue=configuration.getGlue();
		  String tags = configuration.getTags();
	 
	  System.setProperty("cucumber.options","--tags "+ tags);
	  System.setProperty("cucumber.options", glue); // make sure to set glue before features
	  System.setProperty("cucumber.options", features);
		  
	  }
	  */ 
	
 }
	 