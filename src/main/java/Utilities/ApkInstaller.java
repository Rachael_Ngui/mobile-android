package Utilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class ApkInstaller {
	static AndroidDriver<AndroidElement> driver;
	@Test
	public static void driverlaunch() throws MalformedURLException {
		File path= new File("src");
		File apk= new File(path,"app-uat-debug (19).apk");
		DesiredCapabilities capability= new DesiredCapabilities();
		capability.setCapability(MobileCapabilityType.DEVICE_NAME, "Android_10_API30"); 
		capability.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		capability.setCapability(MobileCapabilityType.APP, apk.getAbsolutePath());
		driver= new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),capability);
		//return driver;
	}

}
