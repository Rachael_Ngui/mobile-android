package Utilities;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class DriverFactory {
	
	static AndroidDriver<AndroidElement> driver;
	/*
	DEVICES
	A71 ->R58N513P8RA
	A20 ->R9AN405KP6J
	CX -> 0329625863001980
	*/
	public static AndroidDriver<AndroidElement> driverlaunch() throws MalformedURLException {
		DesiredCapabilities capability= new DesiredCapabilities();
		capability.setCapability(MobileCapabilityType.DEVICE_NAME, "R58N513P8RA"); 
		capability.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		driver= new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),capability);
		return driver;
	}

}
