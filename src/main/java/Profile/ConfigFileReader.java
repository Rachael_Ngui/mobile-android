package Profile;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	
	private static Properties property;
	private String configFilePath="/home/rachael/eclipse-workspace/OneEquity/Configurations/subsidiary.properties";
	
	public ConfigFileReader() {
		BufferedReader reader;
		// initializing the reader objects
		try {
		reader = new BufferedReader(new FileReader(configFilePath));
		property= new Properties();
		
		// getting the property file location
		try {
		property.load(reader);
		reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration properties not located at "+configFilePath);
		}

	}
	
	public String getSheet() {
		String sheet= property.getProperty("sheet");
		return sheet;
	}
	public String getTags() {
		String sheet_no=getSheet();
		String tag=null;
		if(sheet_no.contains("0")) {
			tag=property.getProperty("kenya");
		}
		else if(sheet_no.contains("1")) {
			tag=property.getProperty("uganda");
		}
		else if(sheet_no.contains("2")) {
			tag=property.getProperty("tanzania");
		}
		else if(sheet_no.contains("3")) {
			tag=property.getProperty("rwanda");
		}
		else if(sheet_no.contains("4")) {
			tag=property.getProperty("sudan");
		}
		else if(sheet_no.contains("5")) {
			tag=property.getProperty("drc");
		}
		else {
			System.out.println("Configuration not found in subsidiary.properties file");
		}
		
		return tag;
	}
	public String getFeatures() {
		String features=property.getProperty("features");
		return features;
	}
	public String getGlue() {
		String glue=property.getProperty("glue");
		return glue;
	}

}
